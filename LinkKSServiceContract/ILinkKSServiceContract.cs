﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using LinkKSDTO;

namespace LinkKSServiceContract
{
    [ServiceContract] // Service Contract ı olduğunu bildirmem lazım
    public interface ILinkKSServiceContract
    {
        [OperationContract]
        Guid UserSignUp(string name, string eMail, string password);
       
        [OperationContract]
        DTO_USER GetUserProfile(Guid userId);

        [OperationContract]
        Guid? UserSignIn(string eMail, string password);

        [OperationContract]
        void UserUpdate(Guid userId, string name, string eMail, string password);

        [OperationContract]
        string LinkShorten(string longLink, Guid? userId, string password, DateTime? expiredDate, bool oneShot,
            bool notification, byte status);

        [OperationContract]
        string LinkCheckPassword(Guid linkId, string password);

        [OperationContract]
        void UpdateLinkPassword(Guid linkId, string password);

        [OperationContract]
        void UpdateLinkExpireDate(Guid linkId, DateTime? expireDate);

        [OperationContract]
        void UpdateLinkNotification(Guid linkId, bool notification);

        [OperationContract]
        void UpdateLinkOneShot(Guid linkId, bool oneshot);

        [OperationContract]
        DTO_LINK[] LinkList(Guid userId);

        //string GetPassword(Guid linkId);

        [OperationContract]
        void LinkDelete(Guid linkId);

        [OperationContract]
        DTO_LINK_LOG[] ListLinkLog(Guid linkId);

        [OperationContract]
        DTO_LINK_CONTROL_RESULT GetLong_Link(string shortLink);

        [OperationContract]
        void LinkLog(Guid linkId, string Ip, string Agent, string Referrer);

        [OperationContract]
        void DeleteLink(Guid linkId);
    }
}
