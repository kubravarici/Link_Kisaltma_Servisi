﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using LinkKSServiceContract;
using LinkKS.Business;
using LinkKSDTO;

namespace LinkKSService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "LinkKSService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select LinkKSService.svc or LinkKSService.svc.cs at the Solution Explorer and start debugging.
    public class LinkKSService : ILinkKSServiceContract
    {

        public void LinkLog(Guid linkId, string Ip, string Agent, string Referrer)
        {
            var link = new Link();

            link.Log(linkId,Ip,Agent,Referrer);
        }
        public DTO_LINK[] LinkList(Guid userId)
        {
            var link = new Link();

            return link.List(userId);
        }

        public string LinkCheckPassword(Guid linkId, string password)
        {
            var link = new Link();
            return link.CheckPassword(linkId,password);

        }
        //string GetPassword(Guid linkId);

        public void LinkDelete(Guid linkId)
        {
            var link = new Link();

            link.Delete(linkId);
        }

        public DTO_LINK_LOG[] ListLinkLog(Guid linkId)
        {
            var link = new Link();

            return link.ListLog(linkId);
        }

        public DTO_LINK_CONTROL_RESULT GetLong_Link(string shortLink)
        {
            var link = new Link();

            return link.GetLongLink(shortLink);
        }

        public Guid? UserSignIn(string eMail, string password)
        {
            var user = new User();

            return user.SignIn(eMail, password);
        }

        public Guid UserSignUp(string name, string eMail, string password)
        {
            var user = new User();

            return user.SignUp(name, eMail, password);
        }

        public void UserUpdate(Guid userId, string name, string eMail, string password)
        {
            var user = new User();

            user.Update(userId, name, eMail, password);
        }

        public DTO_USER GetUserProfile(Guid userId)
        {
            var user = new User();

            return user.GetProfile(userId);
        }

        public string LinkShorten(string longLink, Guid? userId, string password, DateTime? expiredDate, bool oneShot,
            bool notification, byte status)
        {
            var link = new Link();

            return link.Shorten(longLink, userId, password, expiredDate, oneShot, notification, status);
        }

        public void UpdateLinkPassword(Guid linkId, string password)
        {
            var link = new Link();

            link.UpdatePassword(linkId, password);
        }

        public void UpdateLinkExpireDate(Guid linkId, DateTime? expireDate)
        {
            var link = new Link();

            link.UpdateExpireDate(linkId, expireDate);
        }

        public void UpdateLinkNotification(Guid linkId, bool notification)
        {
            var link = new Link();

            link.UpdateNotification(linkId, notification);
        }

        public void UpdateLinkOneShot(Guid linkId, bool oneshot)
        {
            var link = new Link();

            link.UpdateOneShot(linkId, oneshot);
        }

        public void DeleteLink(Guid linkId)
        {
            var link = new Link();

            link.Delete(linkId);
        }
    }
}
