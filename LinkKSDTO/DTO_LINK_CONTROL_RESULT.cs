﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkKSDTO
{
    public class DTO_LINK_CONTROL_RESULT
    {
        public Guid LinkId { get; set; }
        public string LongLink { get; set; }
        public bool Password { get; set; }
        public bool OneShot { get; set; }

    }
}
