﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkKSDTO
{
    public enum Status
    {
         Active = 0,
         Deleted = 1
    }
}
