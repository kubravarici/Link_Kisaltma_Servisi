﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkKSDTO
{
    public class DTO_LINK
    {
        public Guid Id { get; set; }
        public string ShortLink { get; set; }
        public string LongLink { get; set; }
        public string Password { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public bool OneShot { get; set; }
        public bool Notification { get; set; }
        public int ClickCount { get; set; }
        public DateTime CreateDate { get; set; }
        public Status Status { get; set; }
    }
}
