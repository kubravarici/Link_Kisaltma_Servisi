﻿using System;
using System.Collections;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LinkKSDTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Win.UI.Models;

namespace Win.UI.Controllers
{
    public class AccountController : BaseController
    {
        [HttpGet]
        public ActionResult UserSignUp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserSignUp(UserSignUp model)
        {
            Servis().UserSignUp(model.Name, model.Email, model.Password);

            return RedirectToAction("UserSignIn", "Account");
        }

        [HttpGet]
        public ActionResult UserSignIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserSignIn(UserSignIn model)
        {
            var UserId = Servis().UserSignIn(model.Email, model.Password);
            if (UserId == null)
            {
                return View();
            }
            HttpCookie UserCookie = new HttpCookie("User", UserId.ToString());
            Response.SetCookie(UserCookie);
            Response.Cookies.Add(UserCookie);
            return RedirectToAction("UyeLinkShorten", "Home");
        }

        [HttpGet]
        public ActionResult LinkList(LinkList model)
        {
            HttpCookie cookie = Request.Cookies["User"];
            if (cookie == null)
            {
                return RedirectToAction("UserSignIn", "Account");
            }
            var liste = Servis().LinkList(new Guid(cookie.Value));
            ViewBag.liste = liste;
            return View(model);
        }

        [HttpGet]
        public ActionResult LinkLogList(LinkLogList model, string id)
        {
            var logliste = Servis().ListLinkLog(new Guid(id));
            ViewBag.logliste = logliste;
            return View(model);
        }

        [HttpGet]
        public ActionResult UserUpdate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UserUpdate(UserUpdate model)
        {
            HttpCookie cookie = Request.Cookies["User"];
            if (cookie == null)
            {
                return RedirectToAction("UserSignIn", "Account");
            }
            Servis().UserUpdate(new Guid(cookie.Value), model.name, model.eMail, model.password);
            return RedirectToAction("UyeLinkShorten", "Home");
        }

        public ActionResult SignOut()
        {
            if (Request.Cookies["User"] != null)
            {
                HttpCookie myCookie = new HttpCookie("User");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }
            return RedirectToAction("LinkShorten","Home");
        }
    }
}