﻿using LinkKS.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Win.UI.Models;

namespace Win.UI.Controllers
{
    public class HomeController : BaseController
    {
      
        [HttpGet]
        public ActionResult LinkShorten()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LinkShorten(LinkShorten model)
        {
            var result = Servis()
                .LinkShorten(model.LongLink, model.UserId, model.Password, model.ExpiredDate, model.OneShot,
                    model.Notification, model.Status);
            ViewData["KisaLink"] = result;
            return View(model);
        }

        [HttpGet]
        public ActionResult UyeLinkShorten()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UyeLinkShorten(LinkShorten model)
        {
            HttpCookie cookie = Request.Cookies["User"];
            var result = Servis()
                .LinkShorten(model.LongLink, new Guid(cookie.Value), model.Password, model.ExpiredDate, model.OneShot,
                    model.Notification, model.Status);
            ViewData["KisaLink"] = result;
            return View(model);
        }

        [HttpGet]
        public ActionResult UpdatePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdatePassword(UpdatePassword model, string id)
        {
            
            Servis().UpdateLinkPassword(new Guid(id),model.Password);
            return RedirectToAction("LinkList", "Account");
        }

        [HttpGet]
        public ActionResult UpdateExpireDate()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UpdateExpireDate(UpdateExpireDate model)
        {
            Servis().UpdateLinkExpireDate(model.LinkId, model.ExpireDate);
            return RedirectToAction("LinkList", "Account");
        }

        [HttpGet]
        public ActionResult UpdateNotification(Guid id)
        {
            LinkKSDataContext dc = new LinkKSDataContext();
            var Notification = dc.LINKs.Where(c => c.ID == id).Select(c => c.NOTIFICATION).FirstOrDefault();
            ViewBag.Notification = Notification;
            return View();
        }

        [HttpPost]
        public ActionResult UpdateNotification(UpdateNotification model, string id)
        {
            Servis().UpdateLinkNotification(new Guid(id), model.Notification);
            return RedirectToAction("LinkList", "Account");
        }

        [HttpGet]
        public ActionResult UpdateOneShot(Guid id)
        {
            LinkKSDataContext dc = new LinkKSDataContext();
            var OneShot = dc.LINKs.Where(c => c.ID == id).Select(c => c.ONE_SHOT).FirstOrDefault();
            ViewBag.OneShot = OneShot;
            return View();
        }

        [HttpPost]
        public ActionResult UpdateOneShot(UpdateOneShot model, string id)
        {
            Servis().UpdateLinkOneShot(new Guid(id), model.OneShot);
            return RedirectToAction("LinkList", "Account");
        }

        [HttpGet]
        public ActionResult CheckPassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CheckPassword(CheckPassword model)
        {

            var lonklink = Servis().LinkCheckPassword(new Guid( Request.QueryString["id"]), model.Password);
            if (lonklink == null)
            {
                return null;
            }
            else
            {
                Request.Cookies.Add(new HttpCookie(Request.QueryString["id"], "1"));
                return Redirect(lonklink);
            }
            
        }

        public ActionResult DeleteLink(string id)
        {
            Servis().DeleteLink(new Guid(id));
            return RedirectToAction("LinkList","Account");
        }
    }
}
