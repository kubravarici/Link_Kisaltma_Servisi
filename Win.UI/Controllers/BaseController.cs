﻿using LinkKSServiceContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;

namespace Win.UI.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        public ILinkKSServiceContract Servis()
        {
            var binding = new BasicHttpBinding();
            var address = new EndpointAddress("http://localhost:49840/LinkKSService.svc");
            var channel = ChannelFactory<ILinkKSServiceContract>.CreateChannel(binding, address);
           
            return channel;
        }
    }
}