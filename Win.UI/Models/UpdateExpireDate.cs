﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Win.UI.Models
{
    public class UpdateExpireDate
    {
        public Guid LinkId { get; set; }
        public DateTime ExpireDate { get; set; }
    }
}