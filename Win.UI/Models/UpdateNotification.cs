﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Win.UI.Models
{
    public class UpdateNotification
    {
        public Guid LinkId { get; set; }
        public bool Notification { get; set; }
    }
}