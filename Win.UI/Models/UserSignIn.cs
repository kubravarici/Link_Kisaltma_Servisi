﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Win.UI.Models
{
    public class UserSignIn
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}