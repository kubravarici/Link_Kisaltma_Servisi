﻿using LinkKSDTO;
using System;
using System.Linq;
using System.Web;

namespace Win.UI.Models
{
    public class LinkList
    {
        
        public Guid Id { get; set; }
        public string ShortLink { get; set; }
        public string LongLink { get; set; }
        public string Password { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public bool OneShot { get; set; }
        public bool Notification { get; set; }
        public int ClickCount { get; set; }
        public DateTime CreateDate { get; set; }
        public Status Status { get; set; }
        public string Icon { get; set; }
    }
}