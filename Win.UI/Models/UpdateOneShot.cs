﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Win.UI.Models
{
    public class UpdateOneShot
    {
        public Guid LinkId { get; set; }
        public bool OneShot { get; set; }
    }
}