﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Win.UI.Models
{
    public class LinkShorten
    {
        public string LongLink { get; set; }
        public Guid? UserId { get; set; }
        public string Password { get; set; }
        public DateTime? ExpiredDate { get; set; }
        public bool OneShot { get; set; }
        public bool Notification { get; set; }
        public byte Status { get; set; }
    }
}