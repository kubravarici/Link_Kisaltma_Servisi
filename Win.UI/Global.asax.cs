﻿using LinkKS.Business;
using LinkKSServiceContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Win.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_BeginRequest()
        {

            var binding = new BasicHttpBinding();
            var address = new EndpointAddress("http://localhost:49840/LinkKSService.svc");
            var channel = ChannelFactory<ILinkKSServiceContract>.CreateChannel(binding, address);

            var segments = this.Request.Url.Segments;

            if (segments.Length != 2 || segments[1].Length != 6 ||
                segments[1].Any(c => !"0123456789abcdefghijklmnoprstuwvxyz".Contains(c)))
            {
                return;
            }

            var shortLink = segments[1];

           
            var kontrolResult = channel.GetLong_Link(shortLink);
            

            if (kontrolResult == null) // Link bulunamadı
            {
                return;
            }

            if (kontrolResult.OneShot)
            {
                if (!this.Request.Cookies.AllKeys.Contains(shortLink))
                {
                    return;
                }
            }

            if (kontrolResult.Password)
            {
                this.Response.Redirect("Home/CheckPassword?id=" + kontrolResult.LinkId + "&shortLink=" + shortLink);

            }

            this.Request.Cookies.Add(new HttpCookie(shortLink, "1"));

            //// TODO Log

            var Agent = this.Request.UserAgent;
            var IP = this.Request.UserHostAddress; // IP
            var Referrer = this.Request.UrlReferrer?.AbsoluteUri; // Referrer

            channel.LinkLog(kontrolResult.LinkId, IP, Agent, Referrer);
           

            this.Response.Redirect(kontrolResult.LongLink);
        }
    }
}
