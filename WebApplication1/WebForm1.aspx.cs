﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var list = new[]
            {
                new { A = 1, Password = "2,", C = 3, D = true, Id=Guid.NewGuid()},
                new { A = 2, Password = "21", C = 31, D = false, Id=Guid.NewGuid()},
                new { A = 3, Password = default(string), C = 32, D = true, Id=Guid.NewGuid()},
                new { A = 4, Password = "23", C = 33, D = true, Id=Guid.NewGuid()},
                new { A = 5, Password = "24", C = 34, D = true, Id=Guid.NewGuid()},
                new { A = 6, Password = "25", C = 35, D = true, Id=Guid.NewGuid()},
            };

            this.G.DataSource = list;
            this.G.DataBind();
        }
    }
}