﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView runat="server" ID="G" AutoGenerateColumns="False">
                <Columns>
                    <asp:BoundField DataField="A" HeaderText="Column A" />
                    <asp:BoundField DataField="D" HeaderText="Column d" />
                    <asp:CheckBoxField DataField="D" HeaderText="CBF" />
                    <asp:HyperLinkField DataTextField="Password" DataNavigateUrlFields="Id" DataNavigateUrlFormatString="password.aspx?id={0}" />
                    <asp:TemplateField HeaderText="Passrowd">
                        <ItemTemplate>
                            <%# Eval("Password") %> <a href="password.aspx?id=<%# Eval("Id") %>"><%# (Eval("Password") == null ? "parola ekle" : "<img src='asdf' alt='düzenle' />") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
