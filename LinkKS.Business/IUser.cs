﻿using LinkKSDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkKS.Business
{
    public interface IUser
    {
        Guid SignUp(string name, string eMail, string password);
        Guid? SignIn(string eMail, string password);
        void Update(Guid userId, string name, string eMail, string password);
       //DTO_USER GetProfile(Guid userId);
    }
}
