﻿using LinkKSDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkKS.Business
{
    public class User : IUser
    {
        public DTO_USER GetProfile(Guid userId)
        {
            using (var dc = new LinkKSDataContext())
            {
                return dc.USERs.Where(c => c.ID == userId).Select(c => new DTO_USER
                {
                    Name = c.NAME,
                    EMail = c.E_MAIL,
                    Password = c.PASSWORD
                }).FirstOrDefault();
            }
        }
        public Guid SignUp(string name, string eMail, string password)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = new USER(); //User tablosunda boş bir satır oluşturduk

                item.ID = Guid.NewGuid();
                item.NAME = name;
                item.E_MAIL = eMail;
                item.PASSWORD = password;
                item.CREATE_DATE = DateTime.Now;


                dc.USERs.InsertOnSubmit(item);

                dc.SubmitChanges();

                return item.ID;

            }
        }

        public Guid? SignIn(string eMail, string password)
        {
            using (var dc = new LinkKSDataContext())
            {
                /*var id =
                    dc.USERs.Where(c => c.E_MAIL == eMail && c.PASSWORD == password)
                        .Select(c => c.ID)
                        .FirstOrDefault();*/

                var id =(from c in dc.USERs where c.E_MAIL==eMail select c.ID).FirstOrDefault();

                if (id == Guid.Empty)
                {
                    return null;
                }

                return id;
            }
        }

        public void Update(Guid userId, string name, string eMail, string password)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.USERs.First(c => c.ID == userId);

                item.NAME = name;
                item.E_MAIL = eMail;
                item.PASSWORD = password;

                dc.SubmitChanges();
            }
        }
    }
}
