﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkKSDTO;

namespace LinkKS.Business
{
    interface ILink
    {
        string Shorten(string longLink, Guid? userId, string password, DateTime? expiredDate, bool oneShot, bool notification, byte status);
        void UpdatePassword(Guid linkId, string password);
        void UpdateExpireDate(Guid linkId, DateTime? expireDate);
        void UpdateNotification(Guid linkId, bool notification);
        void UpdateOneShot(Guid linkId, bool oneShot);
        DTO_LINK[] List(Guid userId);
        //string GetPassword(Guid linkId);
        void Delete(Guid linkId);
        DTO_LINK_LOG[] ListLog(Guid linkId);
        DTO_LINK_CONTROL_RESULT GetLongLink(string shortLink);
        string CheckPassword(Guid linkId, string password);
        void Log(Guid linkId, string Ip, string Agent, string Referrer);
    }
}
