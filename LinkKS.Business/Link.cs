﻿using LinkKSDTO;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkKS.Business
{
    public class Link : ILink
    {
        public string CheckPassword(Guid linkId, string password)
        {
            using (var dc = new LinkKSDataContext())
            {
                return dc.LINKs.Where(c => c.ID == linkId && c.PASSWORD == password).Select(c => c.LONG_LINK).FirstOrDefault();
            }
        }

        public void Delete(Guid linkId)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.Where(c => c.ID == linkId).Select(c => c);

                foreach (var details in item)
                {
                    details.STATUS = 1;
                }
                
                dc.SubmitChanges();

            }
        }

        public DTO_LINK_LOG[] ListLog(Guid linkId)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINK_LOGs.Where(c => c.LINK_ID == linkId).Select(c => new DTO_LINK_LOG
                {
                    IP = c.IP,
                    Agent = c.AGENT,
                    Referrer = c.REFERRER,
                    Date = c.DATE

                }).ToArray();

                return item;
            }
        }

        public DTO_LINK[] List(Guid userId)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.Where(c => c.USER_ID == userId).Select(c => new DTO_LINK
                {
                    Id = c.ID,
                    ShortLink = c.SHORT_LINK,
                    LongLink = c.LONG_LINK,
                    CreateDate = c.CREATE_DATE,
                    ExpiredDate = c.EXPIRE_DATE,
                    Notification = c.NOTIFICATION,
                    OneShot = c.ONE_SHOT,
                    Password = c.PASSWORD,
                    Status = (Status)c.STATUS,
                    ClickCount = c.LINK_LOGs.Count
                }).ToArray();

                return item;

            }
        }

        public DTO_LINK_CONTROL_RESULT GetLongLink(string shortLink)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.Where(c => c.SHORT_LINK == shortLink).
                    Select(c => new
                    {
                        c.ID,
                        c.LONG_LINK,
                        c.STATUS,
                        c.PASSWORD,
                        c.EXPIRE_DATE,
                        c.ONE_SHOT,
                        clicked = c.LINK_LOGs.Any()
                    }).FirstOrDefault();

                if (item == null)
                {
                    return null;
                }

                if (item.STATUS != (byte)Status.Active)
                {
                    return null;
                }

                if (item.EXPIRE_DATE < DateTime.Now)
                {
                    return null;
                }

                return new DTO_LINK_CONTROL_RESULT
                {
                    LinkId = item.ID,
                    LongLink = item.LONG_LINK,
                    Password = item.PASSWORD != null,
                    OneShot = item.ONE_SHOT && item.clicked
                };
            }
        }

        public string Shorten(string longLink, Guid? userId, string password, DateTime? expiredDate, bool oneShot, bool notification, byte status)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = new LINK(); //Link tablosunda boş bir satır oluşturduk

                item.ID = Guid.NewGuid();
                item.LONG_LINK = longLink;
                item.SHORT_LINK = this.GenerateShortLink(dc);
                item.USER_ID = userId;
                item.PASSWORD = password;
                item.EXPIRE_DATE = expiredDate;
                item.ONE_SHOT = oneShot;
                item.NOTIFICATION = notification;
                item.STATUS = status;
                item.CREATE_DATE = DateTime.Now;

                dc.LINKs.InsertOnSubmit(item);

                dc.SubmitChanges();

                return item.SHORT_LINK;
            }
        }

        private const string ValidChars = "0123456789abcdefghijklmnoprstuwvxyz";

        /* private string GenerateShortLink()
         {
             var rnd = new Random();
             var shortLink = "";

             for (int i = 0; i < 6; i++)
             {
                 var index = rnd.Next(Link.ValidChars.Length);

                 shortLink += Link.ValidChars[index];
             }

             return shortLink;
         }*/

        private string GenerateShortLink(LinkKSDataContext dc)
        {
            var rnd = new Random();

            while (true)
            {
                var shortLink = "";

                for (int i = 0; i < 6; i++)
                {
                    var index = rnd.Next(Link.ValidChars.Length);

                    shortLink += Link.ValidChars[index];
                }

                if (!dc.LINKs.Any(c => c.SHORT_LINK == shortLink))
                {
                    return shortLink;
                }

            }

        }

        public void UpdatePassword(Guid linkId, string password)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.First(c => c.ID == linkId);

                item.PASSWORD = password;

                dc.SubmitChanges();
            }
        }

        public void UpdateExpireDate(Guid linkId, DateTime? expireDate)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.First(c => c.ID == linkId);

                item.EXPIRE_DATE = expireDate;

                dc.SubmitChanges();
            }
        }

        public void UpdateNotification(Guid linkId, bool notification)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.First(c => c.ID == linkId);

                item.NOTIFICATION = notification;

                dc.SubmitChanges();
            }
        }

        public void UpdateOneShot(Guid linkId, bool oneShot)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = dc.LINKs.First(c => c.ID == linkId);

                item.ONE_SHOT = oneShot;

                dc.SubmitChanges();
            }
        }

        public void Log(Guid linkId, string Ip, string Agent, string Referrer)
        {
            using (var dc = new LinkKSDataContext())
            {
                var item = new LINK_LOG();

                item.ID = Guid.NewGuid();
                item.LINK_ID = linkId;
                item.IP = Ip;
                item.AGENT = Agent;
                item.REFERRER = Referrer;
                item.DATE = DateTime.Now;

                dc.LINK_LOGs.InsertOnSubmit(item);
                
                dc.SubmitChanges();
            }
        }
    }
}
